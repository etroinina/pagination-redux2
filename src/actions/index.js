import * as constants from './constants';
import { createAction } from '../helpers/actions';

export const setPage = createAction(constants.SET_PAGE);
export const changeValue = createAction(constants.CHANGE_VALUE);