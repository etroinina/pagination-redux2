import React, { useEffect } from 'react';
import { NavLink, withRouter } from "react-router-dom";

const Component = (props) => {
    const { paginationInfo, setPage } = props;
    const { totalPages, currentPage, pagesToShow } = paginationInfo;
    const pageItems = [];

    const start = Math.floor(currentPage / pagesToShow) *pagesToShow;
    const end = Math.min(start + pagesToShow, totalPages);

    useEffect(() => {
        const {pathname} = props.history.location
        const currentRoutePage = parseInt(pathname.replace('/', '')) - 1
        if(currentRoutePage !== currentPage) {
            setPage(currentRoutePage)
        }
    }, [])

    for (let i = start; i < end; i++ ) {
        pageItems.push(
            <li
                className={`item ${(i === currentPage) ? 'current-item': ''}`}
                key={ i }
                onClick={ () => {
                setPage(i);
                }}
            >
                <NavLink to={`/${ i + 1 }`}>
                    { i + 1 }
                </NavLink>
            </li>
        )
    }

    return (

        <ul className="pagination">
            <li>
                <button
                    disabled={currentPage === 0}
                    onClick={() => setPage(0)}>
                    First
                </button>
            </li>
            <li>
                <button
                    disabled={currentPage === 0}
                    onClick={() => setPage(currentPage - 1)}>
                    Prev
                </button>
            </li>

            {pageItems}

            <li>
                <button
                    disabled={currentPage === (totalPages - 1)}
                    onClick={() => setPage(currentPage + 1)}>
                    Next
                </button>
            </li>
            <li>

                <button
                    disabled={currentPage === (totalPages - 1)}
                    onClick={() => setPage(totalPages - 1)}>
                    Last
                </button>

            </li>
        </ul>
    );
};

export const Pagination = withRouter(Component)