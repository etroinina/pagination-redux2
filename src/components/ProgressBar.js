import React from 'react';

export const ProgressBar = (props) => {
    const { percent, changeValue } = props;

    return (
        <div className="progressBarWrapper">
            <button className="progressButton"
                disabled={percent === 0}
                onClick={() => changeValue(percent - 10)}>
                -
            </button>
            <div className="progressBarBlock">
                <div className="progressColor" style={{ width: percent + '%' }}></div>
            </div>
            <button className="progressButton"
                disabled={percent === 100}
                onClick={() => changeValue(percent + 10)}>
                +
            </button>
        </div>
    );
};