import { createSelector } from 'reselect';

export const paginationSelector = createSelector(
    state => state.paginationInfo,
    paginationInfo => paginationInfo
);

export const data = createSelector(
    paginationSelector,
    paginationSelector => paginationSelector.totalPages
);

export const progressSelector = createSelector(
    state => state.progressBar.percent,
    percent => percent
);