import { SET_PAGE } from "../actions/constants";

const initialState = {
    totalPages: 20,
    currentPage: 0,
    pagesToShow: 5
};

export const paginationInfo = (state = initialState, action) => {
    const { type, payload } = action;

    switch (type) {
        case SET_PAGE:
            return { ...state, currentPage: payload };
        default:
            return state;
    }
};