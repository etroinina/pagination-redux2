import { CHANGE_VALUE } from "../actions/constants";

const initialState = {
    percent: 20
};

export const progressBar = (state = initialState, action) => {
    const { type, payload  } = action;

    switch (type) {
        case CHANGE_VALUE:
            return { ...state, percent: payload };
        default:
            return state;
    }
};