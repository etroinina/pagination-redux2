import { paginationInfo } from './paginationInfo';
import { progressBar } from './progressBar';
import { combineReducers } from 'redux';

export const reducer = combineReducers({
    paginationInfo,
    progressBar
});