import React, {Component} from 'react';
import './App.css';
import { Pagination } from "./components/Pagination";
import { ProgressBar } from "./components/ProgressBar";
import { appConnector, sidebarConnector } from "./connector";

const PaginationConnected = appConnector(Pagination);
const ProgressConnected = sidebarConnector(ProgressBar);

class App extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="app">
                <div className="wrapper">
                    <div className="sidebar">
                        <h2>Progress Bar</h2>
                        <ProgressConnected />
                    </div>
                    <div className="main">
                        <div className="content"></div>
                        <div className="pagination-wrapper">
                            <PaginationConnected />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default App;