import { connect } from 'react-redux';
import { setPage, changeValue } from './actions/';
import { createSelector } from 'reselect';
import {
    paginationSelector,
    progressSelector
} from './selectors';

const appSelector = createSelector(
    paginationSelector,
    (paginationSelector) => ({
        paginationInfo: paginationSelector
    })
);

const sidebarSelector = createSelector(
    progressSelector,
    (progressSelector) => ({
        percent: progressSelector
    })
);

export const appConnector = connect(appSelector, {
    setPage
});

export const sidebarConnector = connect(sidebarSelector, {
    changeValue
});


